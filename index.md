---
layout: default
---

# [Public Discussion on Publishing Reform](https://gitlab.com/publishing-reform/discussion/issues)
### [Public Forum](https://gitlab.com/publishing-reform/discussion/issues) for scholarly publishing reform towards [Fair Open Access (FOA)](https://gitlab.com/publishing-reform/discussion/tree/master/Fair%20Open%20Access) with focus on action. Member of the [Fair Open Access Alliance](https://www.fairopenaccess.org/who-we-are/about-us/).

#### [Renowned scientists supporting FOA](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/List%20of%20supporters%20of%20Fair%20Open%20Access.md)

#### [How you can help with transformation to FOA](https://gitlab.com/publishing-reform/discussion/blob/master/How%20to%20help.md)

- [**List of all discussions**](https://gitlab.com/publishing-reform/discussion/issues)
- [**Start new discussion**](https://gitlab.com/publishing-reform/discussion/issues/new)
- [**Quick start guide**](#quick-start-guide)
- [**How can this forum save your time**](#how-can-this-forum-save-your-time)
- [**Challenges**](#what-are-the-challenges)
- [**More user help**](#more-user-help)

Founded and managed by [Mark C. Wilson](https://markcwilson.site/) and [Dmitri Zaitsev](https://www.maths.tcd.ie/~zaitsev/) with help of
[the Team](https://gitlab.com/publishing-reform/discussion/blob/master/team.md).


## Our vision
> #### What do we want?
> A fully *interconnected*, global scholarly ecosystem supported by a *wide variety* of *open* publishing models, 
underpinned by sophisticated *linking* of well-curated, 
*interoperable* research *articles* and other outputs, including *data* and *software*.

> --- [Dr Virginia Barbour](http://orcid.org/0000-0002-2358-2440), Director of the [Australasian Open Access Strategy Group](https://aoasg.org.au) 

Our goal is to open scholarly publishing to society and
raise standards by making it fairer, more efficient and more productive for researchers.
We would like to maximize everyone's efforts and save everyone's time,
including members of this project.

## What makes this forum different?
There are many Question and Answer boards on this and related topics.
We see this forum as Collaborative Action Group aiming to differ in its focus:
- Focus on concrete tangible actions towards the cause.
- Identifying and dispelling concerns standing in the way of action.
- Creating community backed public documents.
- Reporting on all progress made.


## What are the challenges?
A major challenge right now is
to have the academic community agree on the
**seriousness of the harm** caused by the current publishing system.
Please help us with any feedback on the related threads (and adding new):

- [To what extent are paywall journals harming junior researchers?](https://gitlab.com/publishing-reform/discussion/issues/17)
- [Is what happened to Springer's Journal "K-theory" an evidence that Springer does not ensure its journals' preservation?](https://gitlab.com/publishing-reform/discussion/issues/22)
- [Editor accepts paper without waiting from the referee -- quoting Springer's confusing online system](https://gitlab.com/publishing-reform/discussion/issues/29)

## Quick start guide
Check out the [list of all discussions](https://gitlab.com/publishing-reform/discussion/issues). Ignore everything in the left sidebar except 
Setting/Members (if you want to see who else is on the forum) and Repository/Files (if you want to see documents being worked on, although these are usually linked from issues). 
Sort issues via the dropdown menu near the top right: Last Updated is a good way to sort, but there are others. Search (middle, near top) if you want more precision (usually not needed). If your topic of interest is not already discussed in the forum, [start a new issue](https://gitlab.com/publishing-reform/discussion/issues/new). 

## How can this forum save your time?


### Selected participation
You can select discussion threads where you want to participate and get email notifications.
Unlike email lists, where you would receive all the correspondence,
here you can selectively subscribe to or unsubscribe from any thread.

### Notifications
You are automatically subscribed to any new thread you start or add comments to.
That means, you receive any new comments by email.
However, you can unsubscribe from any thread at any time
by simply clicking the "unsubscribe" link in your notification email.

To inspect or change the subscription status, go to any thread,
e.g. [this one](https://gitlab.com/publishing-reform/discussion/issues/2),
where on the right panel at the bottom (you might need to scroll)
you see the Notification tab that you can toggle between Subscribe and Unsubscribe.

There is no current (daily, weekly) "digest" mode, so if you want to know about new threads, you need to change your notification settings (see next section) or check in via the web interface regularly (we recommend at least weekly).

### Global notification settings
If you would like to receive notification for every new thread,
you can [go to the main project page](https://gitlab.com/publishing-reform/discussion)
and click on the "Global" pull-down menu with the bell icon, on the very right tab right below the description.
There you can change "Global" to other values,
for instance to "Watch" to receive email notifications for any activity.
You can even selectively customize the settings by choosing the "Custom" option at the bottom of the menu.

See here for more information about Gitlab Notifications: https://docs.gitlab.com/ee/workflow/notifications.html#notification-settings

### Email communication

Instead of logging to the project, you can do all the communication by email.
You can start a discussion by email (click the link "Email a new issue to this project" under
[the list of all threads](https://gitlab.com/publishing-reform/discussion/issues)
or reply to any notification email to add a comment to the discussion.

See here for [more information about Email Notifications] 
(https://docs.gitlab.com/ee/workflow/notifications.html#gitlab-notification-emails).

### Explicit mention
By writing "@" in front of any member username, you can explicitly mention this member in any discussion,
as [in this example](https://gitlab.com/publishing-reform/working-group/issues/1#note_57216096).
The member will automatically receive an email notification, which is less formal than a direct email.
The receiver can then reply directly to the email or click the Gitlab link and comment within the project.

See here for [more information about Discussion Mentions]
(https://docs.gitlab.com/ee/user/project/issues/issues_functionalities.html#13-mentions).


## More User Help

### Reading discussion threads

To [see the list of all discussions](https://gitlab.com/publishing-reform/discussion/issues),
click the `Issues` tab from the left navigation bar (4th icon from the top).
You can search, filter and sort the discussions there.

When you see any discussion of interest, you can enter it by clicking and then add your comment at the bottom.
You can also subscribe to the updates for any thread by selecting "Subscribe" in the Notification bar
at the very bottom on the right navigation panel (you may need to scroll down).

### Opening a new discussion

To [start a new discussion thread](https://gitlab.com/publishing-reform/discussion/issues/new),
click the `Issues` tab from the left navigation bar (4th icon from the top) and then the green button "New issue"
in the right top corner. Alternatively, from [the main project page](https://gitlab.com/publishing-reform/discussion),
you can click the `+` in the navigation bar right below the description, and then select "New issue".
Simply add the title and description and click on the green button "Submit issue".

### Files
All files are shown on [the main project page](https://gitlab.com/publishing-reform/discussion).
(At the time of writing there is only this README.md file.)
You can dowload the entire file collection with a single click
on the download icon (last to the right of the "Find file" button).

To upload a new file, click on the pull-down menu `+` on [the main project page](https://gitlab.com/publishing-reform/discussion)
in the navigation bar above (3rd navigation row below the description) and select `Upload file`.

### Formatting
For advanced formatting, title/subtitle structure, itemization, images, videos, mathematics formulas and much more, use the [Markdown language](https://docs.gitlab.com/ee/user/markdown.html).
