See jekyll [documentation](https://jekyllrb.com/docs/home/)
to learn about how site is built and how to extend it
 
For simple edits, `index.md` is the landing page, all markdown syntax edits will affect it.

CI takes about a minute to process any changes to the site.